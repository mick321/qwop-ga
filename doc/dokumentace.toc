\select@language {czech}
\contentsline {section}{\numberline {1}\'Uvod}{3}{section.1}
\contentsline {section}{\numberline {2}Genetick\'y algoritmus}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Reprezentace jedince}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Pou\v zit\'a fitness funkce}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Tvorba generac\IeC {\'\i }}{5}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}K\v r\IeC {\'\i }\v zen\IeC {\'\i } a~mutace}{5}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Volba hodnot}{6}{subsection.2.5}
\contentsline {section}{\numberline {3}Programov\'a dokumentace}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}T\v r\IeC {\'\i }dy programu}{6}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Sn\IeC {\'\i }m\'an\IeC {\'\i } obrazu a~rozpozn\'an\IeC {\'\i } hern\IeC {\'\i }ho stavu}{7}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Simulovan\'e stisky kl\'aves}{8}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Ukl\'ad\'an\IeC {\'\i } jedinc\r u}{9}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Konfigura\v cn\IeC {\'\i } soubor}{9}{subsection.3.5}
\contentsline {section}{\numberline {4}V\'ysledky}{10}{section.4}
\contentsline {section}{\numberline {5}Z\'av\v er}{10}{section.5}
