#pragma once
#include "screencap.h"
#include "runner.h"

enum ProgramState
{
  programStatePrepare,        // prepare runner
  programStateRun,            // test runner
  programStateNewGeneration,  // make new generation
  programStateReplayPrepare,
  programStateReplayRun,
};

/// Class defining behavior of application.
class MainProgram
{
private:
  ScreenCaptureLoop* screenCapture = NULL;
  volatile bool programRunning = true;
  ProgramState programState;

  int     runnerIdSequence = 1;
  int     generationNumber = 1;
  std::vector<Runner*> runners;      //!< population of runners 
  int     activeIndex;               //!< active runner index (runner being tested)
  Runner* bestRunner = NULL;
  int     runCounter = 0;

  int     testNumber;  //!< each runner will be tested more times (game is pretty non-deterministic)
  float   testFitnessMin; //!< each runner will be tested more times - this is worst fitness of all tries

  FILE*   fileReplay = NULL;

  /// Print something nice, greeting or whatever.
  void PrintWelcome();
  /// User calibrates position of QWOP window.
  void CalibrateScreen();
  /// Refresh browser page from time to time.
  void ReloadGame();

  /// Generate random runners. (Add missing to fulfill desired runner count.)
  void GenerateRunners();
  /// Load runners from default data file. If file not found or not enough runners in file, returns false - probably call GenerateRunners to fill up array.
  bool LoadRunners();
  /// Save runners to default data file. Returns false on failure.
  bool SaveRunners();
  /// Clear runners from memory.
  void ClearRunners();
  
  /// Method implementing program state "run".
  void StateRun();
  /// Method implementing program state "prepare". Prepare runner on track.
  void StatePrepare();
  /// Method implementing program state "make new generation".
  void StateNewGeneration();
  /// Method implementing program state "replay-prepare" (next runner).
  void StateReplayPrepare();
  /// Method implementing program state "replay-run" (loaded runner).
  void StateReplayRun();
public:
  /// Constructor.
  MainProgram();
  /// Destructor.
  ~MainProgram();

  /// Run the program.
  int Run();
};