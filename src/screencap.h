#pragma once
#include "utils.h"

class DummyMarker;

/// This class creates separate thread that takes screenshots (only part of screen is actually shot).
class ScreenCaptureLoop
{
private:
  BITMAPINFOHEADER bi;
  HDC hdcScreen = NULL;
  HDC hdcMemDC = NULL;
  HBITMAP hbmScreen = NULL;
  BITMAP bmpScreen;
  unsigned char* hDIB = NULL;
  unsigned char* hDIB_Text = NULL;

  tesseract::TessBaseAPI tessAPI;

  volatile bool threadRunning = true;
  bool initOk = false;           //!< flag set to true if initialization finished properly
  HANDLE threadHandle = NULL;

  DummyMarker* debugMarkHeadPos = NULL;
  DummyMarker* debugMarkBodyPos = NULL;
  DummyMarker* debugMarkScore = NULL;

  /// Capture screen area.
  void CaptureScreen();
  /// Thread entry point.
  static DWORD WINAPI ThreadProcStatic(_In_  LPVOID lpParameter);
  /// Thread entry point (with context of "this").
  DWORD WINAPI ThreadProc();

  GameState        gameState;
  volatile LONG    gameStateLock = 1;
  DWORD            startTime;
  bool             nextRunner = false;

  inline void LockGameState() { while (InterlockedExchange(&gameStateLock, 0) == 0) { } }
  inline void UnlockGameState() { gameStateLock = 1; }

  /// Find head position in bitmap. Given coordinates are normalized x:(-aspect, aspect), y:(-1, 1).
  /// Returns false if not found.
  bool FindHeadInBitmap(float& x, float& y);
  /// Find body position in bitmap. Given coordinates are normalized x:(-aspect, aspect), y:(-1, 1).
  /// Returns false if not found.
  bool FindBodyInBitmap(float headPosX, float headPosY, float& x, float& y);
  /// Read score from bitmap.
  /// Returns false if unreadable.
  bool ReadScoreFromBitmap(float& score);
  /// Returns true if game is over.
  bool DetectGameOver();
public:
  /// Constructor. Initializes all necessary objects.
  ScreenCaptureLoop();
  /// Destructor. Waits for the thread to finish.
  ~ScreenCaptureLoop();
  /// Start the thread. 
  bool Start();

  /// Get current game state.
  GameState GetGameState();
  /// Next runner! Reset start time and stored game state.
  inline void NotifyNextRunner() { LockGameState(); nextRunner = true; startTime = GetTickCount(); gameState = GameState(); UnlockGameState(); }
};