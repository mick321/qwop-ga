#include "stdafx.h"
#include "bezec.h"
#include "vk_keys.h"


std::map<int, bezec> nactene_geny;

bezec make_random()
{
  bezec rtn;
  int gen_length = GEN_START_LENGTH_MIN + rand()%(GEN_START_LENGTH_MAX-GEN_START_LENGTH_MIN+1);
  for (int i = 0; i < gen_length; i++)
    rtn.gen[i] = possible_gens[rand()%5];
  rtn.gen[gen_length] = '\0';
  rtn.dist = -100;
  rtn.average_speed = -100;
  rtn.id = global_iter;
  save_bezec(&rtn);
  return rtn;
}

bezec make_child(bezec* prvni, bezec* druhy)
{
  bezec child;
  int l1 = strlen(prvni->gen), l2 = strlen(druhy->gen);
  int l = min(l1,l2);
  int mutace = 0;
  for (int i = 0; i < l; i++)
  {
    if ((rand()%1000000) <= MUTATION_PROBABILITY_MUL_1000000)
    {
      child.gen[i] = possible_gens[rand()%5];
      mutace++;
    }
    else
      child.gen[i] = (rand()%2) ? prvni->gen[i] : druhy->gen[i];
  }

  if (l < l1) // dokopirovani zbytku genu
  {
    for (int i = l; i < l1; i++)
    {
      if ((rand()%1000000) <= MUTATION_PROBABILITY_MUL_1000000)
      {
        child.gen[i] = possible_gens[rand()%5];
        mutace++;
      }
      else
        child.gen[i] = prvni->gen[i];
    }
    l = l1;
  }
  else
  {
    for (int i = l; i < l2; i++)
    {
      if ((rand()%1000000) <= MUTATION_PROBABILITY_MUL_1000000)
      {
        child.gen[i] = possible_gens[rand()%5];
        mutace++;
      }
      else
        child.gen[i] = druhy->gen[i];
    }
    l = l2;
  }

  if (l < 1000)
  {
    if ((rand()%1000000) <= MUTATION_ADD_PROBABILITY)
    {
      child.gen[l++] = possible_gens[rand()%5];      
    }
  }
  else
    if (l > 5)
    {
      if ((rand()%1000000) <= MUTATION_REM_PROBABILITY)
      {
        child.gen[l-1] = '\0';
      }
    }

  child.gen[l] = '\0';

  printf("\n >> Zrodil se novy bezec!\n >>  gen: %s\n mutace: %d\n", child.gen, mutace);    

  child.dist = -100;
  child.average_speed = -100;
  child.id = global_iter;
  save_bezec(&child);
  return child;
}

int global_iter = 0;

void nacti_iter()
{
  FILE* f;  
  f = fopen("runners/iter.ini", "r");
  fscanf(f,"%d\n",&global_iter);  
  for (int i = 0; i < global_iter; i++)
  {
    fscanf(f,"%s %d %d\n", nactene_geny[i].gen,
      &nactene_geny[i].dist, &nactene_geny[i].average_speed);

    nactene_geny[i].id = i;
  }
  fclose(f);  
}

void save_iter()
{
  FILE* f;  
  f = fopen("runners/iter.ini", "w");
  fprintf(f,"%d\n",global_iter);
  
  for (std::map<int, bezec>::iterator it = nactene_geny.begin();
       it != nactene_geny.end();
       it++)       
     fprintf(f, "%s %d %d\n", it->second.gen, it->second.dist, it->second.average_speed);

  fclose(f);  
}

bezec load_bezec(int index)
{
  bezec rtn;
  
  if (nactene_geny.count(index) == 0)
  {
    rtn = make_random();
    nactene_geny[index].id = index;
    nactene_geny[index] = rtn;
    return rtn;
  }

  return nactene_geny[index];
}

void save_bezec(bezec* utika)
{
  nactene_geny[global_iter] = *utika;  
  nactene_geny[global_iter].id = global_iter;
  global_iter++;
  printf(" -- ulozen gen %s --\n", utika->gen);  
}

#define RUN_CANCEL -100000

int run_bezec(bezec* utika, int (*callbackfunc)(void))
{  
  int current_meters = 0;
  int speed = 1;
  int counter = 0;
  char* sekvence_aktualni;
  int num_null_speed = 0;
  int def_sleep;
  int totalspeed = 0;
  int totalspeedcount = 0;

  sekvence_aktualni = utika->gen;

  stiskni(0);
  stiskni('R'); // RESTART  

  printf("** Na trati je bezec %s (iterace %d) **\n",utika->gen,global_iter);

  if (utika->average_speed != -100 && utika->dist != -100)
  {
    printf("  <pouzivam udaje z cache, beh neni nutne realizovat>\n");
    goto cachedRunJump;
  }
  
  while (num_null_speed < 6) // pocet napocitanych nulovych rychlosti
  {    

    if (GetAsyncKeyState(VK_ESCAPE))
    {
      return RUN_CANCEL;
    }

    stiskni(*sekvence_aktualni);
    sekvence_aktualni++;
    if ((*sekvence_aktualni) == '\0')
      sekvence_aktualni = utika->gen;              
    
    counter++;
    def_sleep = 100;
    if (counter >= 20) // kazdych dve vteriny zjistime novy stav
    {
      counter = 0;
      int lastTime = GetTickCount();
      int i = callbackfunc();
      if (i >= -100 && i <= 1050) 
      { // nova data!
        speed = i-current_meters;
        if (speed <= 0) num_null_speed += 2;
        else if (num_null_speed) num_null_speed--;

        current_meters = i;          
        printf("|%4d", current_meters);
        if (current_meters < 1000)  // rychlost zapocitat pouze, pokud jeste bezi
        {
          totalspeed += speed;
          totalspeedcount++;        
        }        
      }
      else
      {
        printf("| ?? ", current_meters*0.1f);
        num_null_speed++; //bezce, co nam "kazi obraz", nechceme :D
      }
      def_sleep = def_sleep-(GetTickCount()-lastTime);
    }
    Sleep(max(def_sleep,0));
  }

  printf("\n");
  stiskni(0);

  nactene_geny[utika->id].dist = 
    utika->dist = min(current_meters,1000);
  nactene_geny[utika->id].average_speed = 
    utika->average_speed = totalspeed/max(1, totalspeedcount);

  if (utika->dist == 1000)
  {
    printf(" Bezec dobehl do cile! Gratz! \n");    
  }
  
  // misto doskoku pri cached udajich
  cachedRunJump:

  int soucin;
  soucin = utika->dist * utika->average_speed * (utika->dist+utika->average_speed<0 ? -1 : 1);    

  printf("Ubehl: %d , Prumerna rychlost: %d\n"
         "Skore celkem (soucin): %d\n", utika->dist, utika->average_speed,soucin);

  return soucin;
}


void round_bezec(bezec* prvni, bezec* druhy, bezec* treti, int (*callbackfunc)(void))
{
  printf(" -------------- dalsi kolo --------------- \n");  
  int metry[3];
  Sleep(200);
  metry[0] = run_bezec(prvni, callbackfunc);
  if (metry[0] == RUN_CANCEL) return;
  Sleep(200);
  metry[1] = run_bezec(druhy, callbackfunc);
  if (metry[1] == RUN_CANCEL) return;
  Sleep(200);
  metry[2] = run_bezec(treti, callbackfunc);
  if (metry[2] == RUN_CANCEL) return;
  Sleep(200);
  
  if (metry[0] < metry[1]) // prvni je horsi nez druhy  ? 1 ? 2
  {
    if (metry[0] < metry[2]) // a taky nez treti  1 2 3
    {
      *prvni = make_child(druhy, treti);
      printf("prvni hrac byl nahrazen\n");
    }
    else                // treti je nejhorsi  3 1 2
    {
      *treti = make_child(prvni, druhy);
      printf("treti hrac byl nahrazen\n");
    }
  }
  else // prvni je lepsi nez druhy   ? 2 ? 1
  {
    if (metry[1] < metry[2]) // druhy je horsi nez treti   2 3 1
    {
      *druhy = make_child(prvni, treti);
      printf("druhy hrac byl nahrazen\n");
    }
    else // druhy je lepsi nez treti       3 2 1
    {
      *treti = make_child(prvni, druhy);
      printf("treti hrac byl nahrazen\n");
    }
  }
  printf(" ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ \n\n");
  save_iter();
}
