#include "stdafx.h"
#include "vk_keys.h"
#include "program.h"

// ------------------------------------------------------------------------

/// Constructor.
MainProgram::MainProgram()
{
  screenCapture = new ScreenCaptureLoop();
}

/// Destructor.
MainProgram::~MainProgram()
{
  ClearRunners();
  if (screenCapture)
  {
    delete screenCapture;
    screenCapture = NULL;
  }
}

/// Generate random runners. (Add missing to fulfill desired runner count.)
void MainProgram::GenerateRunners()
{
  auto& settings = Settings::GetInstance();
  int rc = settings.runnerCount;
  int rcBefore = (int)runners.size();
  runners.resize(rc);

  for (int i = rcBefore; i < rc; i++)
  {
    runners[i] = Runner::New(runnerIdSequence++);
    runners[i]->Randomize();
  }

  if (rcBefore == 0)
  {
    bestRunner = NULL;
  }
}

/// Load runners from default data file. If file not found or not enough runners in file, returns false - probably call GenerateRunners to fill up array.
bool MainProgram::LoadRunners()
{
  ClearRunners();

  auto& settings = Settings::GetInstance();
  const char* filename;
  if (settings.runnerType == runnerBasic)
  {
    filename = "runners/basic.csv";
  }
  else if (settings.runnerType == runnerAdvanced)
  {
    filename = "runners/advanced.csv";
  }
  else
  {
    THROW_FATAL("Unknown runner type, cannot load runners!");
  }

  FILE* f = fopen(filename, "r");
  int countLoaded = 0;
  if (f)
  {
    Runner* newRunner;
    bool loadOk = true;
    if (fscanf(f, "%d\n", &generationNumber) == 1)
    {
      do
      {
        newRunner = Runner::New(0);
        loadOk = newRunner->Scan(f);
        if (loadOk)
        {
          runners.push_back(newRunner);
          runnerIdSequence = max(1 + newRunner->GetId(), runnerIdSequence);
          float fitness;
          if (newRunner->GetCachedFitness(fitness))
          {
            float bestSoFar;
            if (bestRunner)
            {
              if (bestRunner->GetCachedFitness(bestSoFar) && fitness > bestSoFar)
                bestRunner = newRunner;
            }
            else
            {
              bestRunner = newRunner;
            }
          }
          countLoaded++;
        }
        else
        {
          delete newRunner;
        }
      } while (loadOk && countLoaded < settings.runnerCount);
    }
    else
    {
      loadOk = false;
    }
    fclose(f);
  }

  if (countLoaded < settings.runnerCount)
  {
    fprintf(stderr, "Warning: Cannot load enough runners from file %s.\n", filename);
    return false;
  }
  else
  {
    printf("Runners loaded.\n");
    return true;
  }
}

/// Save runners to default data file. Returns false on failure.
bool MainProgram::SaveRunners()
{
  auto& settings = Settings::GetInstance();
  const char* filename;
  const char* filenameAvgFit;
  if (settings.runnerType == runnerBasic)
  {
    filename = "runners/basic.csv";
    filenameAvgFit = "runners/basicAvg.csv";
  }
  else if (settings.runnerType == runnerAdvanced)
  {
    filename = "runners/advanced.csv";
    filenameAvgFit = "runners/advancedAvg.csv";
  }
  else
  {
    THROW_FATAL("Unknown runner type, cannot save runners!");
  }

  FILE* f = fopen(filename, "w");
  int countSaved = 0;
  float fitnessSum = 0.f;
  int fitnessCount = 0;
  if (f)
  {
    fprintf(f, "%d\n", generationNumber);
    for (auto r : runners)
    {
      if (!r->Print(f))
      {
        fprintf(stderr, "Cannot save runner %d to file %s.\n", r->GetId(), filename);
        break;
      }

      float fitness;
      if (r->GetCachedFitness(fitness))
      {
        fitnessSum += fitness;
        fitnessCount++;
      }

      countSaved++;
    }
    fclose(f);
  }

  f = fopen(filenameAvgFit, "a");
  if (f)
  {
    fprintf(f, "generation;%d;lastGeneratedId;%d;averageFitness;%f\n", generationNumber, runnerIdSequence - 1, fitnessSum / fitnessCount);
    fclose(f);
  }

  if (countSaved != (int)runners.size())
  {
    fprintf(stderr, "Warning: Cannot save all runners to file %s.\n", filename);
    return false;
  }
  else
  {
    return true;
  }
}

/// Clear runners from memory.
void MainProgram::ClearRunners()
{
  for (Runner*& r : runners)
    if (r)
    {
      delete r;
      r = NULL;
    }
  runners.clear();
  bestRunner = NULL;
}

/// Print something nice, greeting or whatever.
void MainProgram::PrintWelcome()
{
  printf("Welcome! Let's play QWOP!\n\n");
}

/// User calibrates position of QWOP window.
void MainProgram::CalibrateScreen()
{
  printf("Before we start, calibration is needed.\n");
  printf("Place cursor above TOP LEFT corner of QWOP game screen and press ENTER.\n");
  
  int i = 0;
  do
  {
    Sleep(50);
  } while (!GetAsyncKeyState(VK_RETURN));

  while (GetAsyncKeyState(VK_RETURN))
  {
    Sleep(0);
  }

  POINT pointTopLeft;
  GetCursorPos(&pointTopLeft);

  Settings::GetInstance().captureX = pointTopLeft.x + Settings::GetInstance().captureOffsetX;
  Settings::GetInstance().captureY = pointTopLeft.y + Settings::GetInstance().captureOffsetY;

  printf("Place cursor above browser refresh button and press ENTER.\n");
  printf("(Game consistently slows down, so we have to restart it from time to time.)\n");

  do
  {
    Sleep(50);
  } while (!GetAsyncKeyState(VK_RETURN));

  while (GetAsyncKeyState(VK_RETURN))
  {
    Sleep(0);
  }

  POINT pointRefresh;
  GetCursorPos(&pointRefresh);

  Settings::GetInstance().refreshButtonX = pointRefresh.x;
  Settings::GetInstance().refreshButtonY = pointRefresh.y;

  printf("Calibrated.\n");
}

/// Refresh browser page from time to time.
void MainProgram::ReloadGame()
{
  PressKey::Click(Settings::GetInstance().refreshButtonX, Settings::GetInstance().refreshButtonY);
  Sleep(200);
  screenCapture->NotifyNextRunner(); 
  while (!screenCapture->GetGameState().valid)
  {
    PressKey::Click(Settings::GetInstance().captureX, Settings::GetInstance().captureY);
    PressKey::PushOnly(" R");
    Sleep(250);
  }
  PressKey::ReleaseAllPushed();

  screenCapture->NotifyNextRunner();
}

/// Run the program.
int MainProgram::Run()
{
  PrintWelcome();
  CalibrateScreen();
  if (!screenCapture->Start())
  {
    fprintf(stderr, "Cannot start module capturing screen.\n");
    return 1;
  }

  if (Settings::GetInstance().replayMode == false)
  {
    if (!LoadRunners())
      GenerateRunners();

    programState = programStatePrepare;
    activeIndex = 0;
  }
  else
  {
    programState = programStateReplayPrepare;
  }

  printf("Running.\n\n");
  while (programRunning)
  {
    if (GetAsyncKeyState(VK_ESCAPE))
      programRunning = false;

    switch (programState)
    {
    case programStatePrepare:
      StatePrepare();
      break;
    case programStateRun:
      StateRun();
      break;
    case programStateNewGeneration:
      StateNewGeneration();
      break;
    case programStateReplayPrepare:
      StateReplayPrepare();
      break;
    case programStateReplayRun:
      StateReplayRun();
      break;
    }
    PressKey::Click(Settings::GetInstance().captureX, Settings::GetInstance().captureY);
    Sleep(Settings::GetInstance().timeBetweenKeyHits);
  }
  PressKey::ReleaseAllPushed();

  if (Settings::GetInstance().replayMode == false)
    if (!SaveRunners())
    {
      printf("Press any key to continue...\n");
      getchar();
    }

  printf("Finished.\n");
  return 0;
}


/// Method implementing program state "run".
void MainProgram::StateRun()
{
  GameState state = screenCapture->GetGameState();
  Runner* active = runners[activeIndex];
  if (state.valid)
  {
    active->Update(state);

    if (!state.gameOver && state.timestamp < Settings::GetInstance().timeMax)
    {
      char keys[5];
      bool keyQ, keyW, keyO, keyP;
      active->GetKeys(keyQ, keyW, keyO, keyP);
      KeysToStr(keyQ, keyW, keyO, keyP, keys);
      PressKey::PushOnly(keys);
      PressKey::Click(Settings::GetInstance().captureX, Settings::GetInstance().captureY);
    }
    else if (testNumber == 0)
    {
      // testing is over
      float fitness = active->GetFitness();
      if (testFitnessMin < fitness) // actually, our runner is worse
        fitness = testFitnessMin;
      active->SetCachedFitness(fitness);

      float bestSoFar;
      if (bestRunner)
      {
        if (bestRunner->GetCachedFitness(bestSoFar) && fitness > bestSoFar)
        {
          bestRunner = active;
          FILE* f;
          switch (Settings::GetInstance().runnerType)
          {
          case runnerBasic: f = fopen("runners/basicBest.csv", "a"); break;
          case runnerAdvanced: f = fopen("runners/advancedBest.csv", "a"); break;
          }
          if (f)
          {
            bestRunner->Print(f);
            fclose(f);
          }
        }
      }
      else
      {
        bestRunner = active;
      }

      printf("Runner #%d stopped after %.1f seconds. %sFitness: %.2f\n", active->GetId(), 0.001f * state.timestamp, 
        bestRunner == active ? "***NEW BEST*** " : "", fitness);

      PressKey::ReleaseAllPushed();
      programState = programStatePrepare;
      activeIndex++;
    }
    else 
    {
      --testNumber;
      // test it once more

      float fitness = active->GetFitness();
      if (fitness < testFitnessMin) // store worst result
        testFitnessMin = fitness;
      active->SetCachedFitness(testFitnessMin);

      PressKey::PushOnly(" R");
      PressKey::ReleaseAllPushed();
      screenCapture->NotifyNextRunner();
      while (!screenCapture->GetGameState().valid)
      {
        PressKey::Click(Settings::GetInstance().captureX, Settings::GetInstance().captureY);
        PressKey::PushOnly(" R");
        Sleep(250);
      }
      PressKey::ReleaseAllPushed();
      Sleep(500);
      screenCapture->NotifyNextRunner();

      active->Reset(screenCapture->GetGameState());
      programState = programStateRun;
      printf("Runner #%d runs again...\n", active->GetId());
    }
  }
}

/// Method implementing program state "prepare". Prepare runner on track.
void MainProgram::StatePrepare()
{
  auto& settings = Settings::GetInstance();
  
  /*
  // do not skip
  float fitness;
  while (activeIndex < (int)runners.size() && runners[activeIndex]->GetCachedFitness(fitness))
  {
    printf("Runner #%d already processed. Fitness: %.1f\n", runners[activeIndex]->GetId(), fitness);
    activeIndex++;
  }
  */

  if (activeIndex >= (int)runners.size())
  {
    printf("Evaluation of generation finished.\n");
    programState = programStateNewGeneration;
    return;
  }

  Runner* active = runners[activeIndex];
  bool reloaded = false;
  if (runCounter % Settings::GetInstance().refreshPeriod == 0)
  {
    ReloadGame();

    int maxwait = 20;
    while (screenCapture->GetGameState().valid && maxwait > 0)
    {
      Sleep(100);
      maxwait--;
    }
    reloaded = true;
  }

  runCounter++;

  testNumber = 3;
  testFitnessMin = FLT_MAX;

  PressKey::PushOnly(" R");
  PressKey::ReleaseAllPushed();
  screenCapture->NotifyNextRunner();
  while (!screenCapture->GetGameState().valid && !GetAsyncKeyState(VK_ESCAPE))
  {
    PressKey::Click(Settings::GetInstance().captureX, Settings::GetInstance().captureY);
    PressKey::PushOnly(" R");
    Sleep(250);
  }
  PressKey::ReleaseAllPushed();
  Sleep(500);

  if (reloaded)
    printf("Reload ok.\n");

  screenCapture->NotifyNextRunner();

  active->Reset(screenCapture->GetGameState());
  programState = programStateRun;
  printf("Generation %d: Runner #%d runs...\n", generationNumber, active->GetId());

}

/// Method implementing program state "make new generation".
void MainProgram::StateNewGeneration()
{
  printf("=== CREATING GENERATION %d ===\n", generationNumber + 1);
  auto& settings = Settings::GetInstance();

  // sort our runners, worse first, best last
  std::sort(runners.begin(), runners.end(), [](Runner* a, Runner* b) 
  { 
    float fitnessA, fitnessB; 
    a->GetCachedFitness(fitnessA); 
    b->GetCachedFitness(fitnessB); 
    return fitnessA < fitnessB; 
  });

  // best fitness -> most places in ladder
  std::vector<Runner*> ladder;
  for (int i = 0; i < (int)runners.size(); i++)
  {
    for (int j = 0; j <= i; j++)
      ladder.push_back(runners[i]);
  }

  // make new generation
  std::vector<Runner*> runnersNew;
  runnersNew.reserve(runners.size());
  for (int i = 0; i < (int)runners.size() - settings.eliteSurviveCount; i++)
  {
    Runner* parents[2];
    parents[0] = ladder[rand() % ladder.size()];
    do
    {
      parents[1] = ladder[rand() % ladder.size()];
    } while (parents[1] == parents[0]);

    Runner* newChild;
    newChild = Runner::New(runnerIdSequence++);
    newChild->CrossoverAndMutate(*parents[0], *parents[1]);

    float fitnessA, fitnessB;
    parents[0]->GetCachedFitness(fitnessA);
    parents[1]->GetCachedFitness(fitnessB);
    printf("#%d  <==  #%d (f=%.2f) + #%d (f=%.2f)\n", newChild->GetId(), parents[0]->GetId(), fitnessA, parents[1]->GetId(), fitnessB);

    runnersNew.push_back(newChild);
  }

  // fill rest with our best runners (elite)
  for (int i = (int)runners.size() - settings.eliteSurviveCount; i < (int)runners.size(); i++)
  {
    runnersNew.push_back(runners[i]);
    float fitness;
    runners[i]->GetCachedFitness(fitness);
    printf("#%d  <==  #%d (f=%.2f)\n", runners[i]->GetId(), runners[i]->GetId(), fitness);
  }
  
  assert(runnersNew.size() == runners.size());

  // delete all runners that did not survive
  for (int i = 0; i < (int)runners.size() - settings.eliteSurviveCount; i++)
  {
    delete runners[i];
    if (runners[i] == bestRunner)
      bestRunner = NULL;
  }

  // swap arrays
  runners.swap(runnersNew);
  generationNumber++;
  programState = programStatePrepare;
  activeIndex = 0;
  printf("=== GENERATION %d ===\n", generationNumber);
  
  SaveRunners();
}

/// Method implementing program state "replay-prepare" (next runner).
void MainProgram::StateReplayPrepare()
{
  if (!fileReplay)
  {
    const char* filename;
    switch (Settings::GetInstance().runnerType)
    {
    case runnerBasic: filename = "runners/basicReplay.csv"; break;
    case runnerAdvanced: filename = "runners/advancedReplay.csv"; break;
    default: THROW_FATAL("Unknown runner type!");
    }

    fileReplay = fopen(filename, "r");
    if (!fileReplay)
    {
      fprintf(stderr, "Cannot open replay file.\n", filename);
      programRunning = false;
      return;
    }
  }

  if (runCounter % Settings::GetInstance().refreshPeriod == 0)
  {
    ReloadGame();
  }
  runCounter++;

  runners.resize(1);
  runners[0] = Runner::New(0);
  if (runners[0]->Scan(fileReplay))
  {
    printf("Replay: Runner #%d runs...\n", runners[0]->GetId());
    float fitness;
    if (runners[0]->GetCachedFitness(fitness))
    {
      printf("  --- his fitness was: %.2f\n", fitness);
    }
    programState = programStateReplayRun;
  }
  else
  {
    delete runners[0];
    runners[0] = NULL;
    runners.clear();
    printf("Replay: No more runners found.\n");
    programRunning = false;
    bestRunner = NULL;
  }
}

/// Method implementing program state "replay-run" (loaded runner).
void MainProgram::StateReplayRun()
{
  GameState state = screenCapture->GetGameState();
  if (state.valid)
  {
    runners[0]->Update(state);

    if (!state.gameOver && state.timestamp < Settings::GetInstance().timeMax)
    {
      char keys[5];
      bool keyQ, keyW, keyO, keyP;
      runners[0]->GetKeys(keyQ, keyW, keyO, keyP);
      KeysToStr(keyQ, keyW, keyO, keyP, keys);
      PressKey::PushOnly(keys);
      PressKey::Click(Settings::GetInstance().captureX, Settings::GetInstance().captureY);
    }
    else
    {
      delete runners[0];
      runners[0] = NULL;
      programState = programStateReplayPrepare;
      bestRunner = NULL;

      PressKey::PushOnly(" R");
      PressKey::ReleaseAllPushed();
      screenCapture->NotifyNextRunner();
      while (!screenCapture->GetGameState().valid)
      {
        Sleep(0);
      }
      screenCapture->NotifyNextRunner();
    }
  }
}

// ------------------------------------------------------------------------

/// Process command line arguments.
int processArgs(int argc, char* argv[])
{
  for (int i = 1; i < argc; i++)
  {
    if (!strcmp(argv[1], "--debug"))
      Settings::GetInstance().debug = true;
    else
    {
      fprintf(stderr, "Unknown program argument: %s\n", argv[i]);
      return -1;
    }
  }
  return 0;
}

/// Application entry point.
int main(int argc, char* argv[])
{
  srand((int)GetTickCount());
  if (processArgs(argc, argv) != 0)
    return -1;

  MainProgram* program = NULL;
  try
  {
    program = new MainProgram();
  }
  catch (FatalException& e)
  {
    delete program;
    printf("%s\n", e.what());
    return -1;
  }

  int rtn = program->Run();
  delete program;

  return rtn;
}
