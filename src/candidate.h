#pragma once

typedef struct BEZEC
{
  int  id;
  char gen[1024];
  int  average_speed;
  int  dist;
}bezec;

const char possible_gens[] = {'Q', 'W', 'O', 'P', 'Z'};

#define GEN_START_LENGTH_MIN 4
#define GEN_START_LENGTH_MAX 8

// v jednom z peti set >>prvku<< genu dojde k mutaci
#define MUTATION_PROBABILITY_MUL_1000000 2000

// v jednom z 100 >>jedincu<< dojde k pridani genu
#define MUTATION_ADD_PROBABILITY 10000
#define MUTATION_REM_PROBABILITY 10000

bezec make_random();
bezec make_child(bezec* prvni, bezec* druhy);
void nacti_iter();
void save_iter();
bezec load_bezec(int index);
void save_bezec(bezec* utika);
int run_bezec(bezec* utika, int (*callbackfunc)());

// souboje mezi temito tremi, nejhorsi je prepsan potomkem dvou lepsich
void round_bezec(bezec* prvni, bezec* druhy, bezec* treti, int (*callbackfunc)(void));

extern int global_iter;

// maximum count of genes
#define MAX_GENE_COUNT 100
// duration of one gene playback
#define GENE_DURATION  250

/// Entire genome of runner.
struct Genome
{
  BYTE  genomeData[MAX_GENE_COUNT];   //!< coded genome
  int   geneCount;      //!< count of stored genes

  inline Genome(const int geneCount)
  {
    if (geneCount > MAX_GENE_COUNT)
      throw std::exception("");

    genomeData = 
  }
};

/// Runner class, has stored its genes.
class Runner
{

public:
  void CrossWith(Runner& other);

};

/// Our runner.
class BetterRunner
{

};