// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <Windows.h>
#include <vector>
#include <map>
#include <set>
#include <assert.h>
#include <algorithm>

#define __MSW32__
#define __IPEREGDLL
#include "../include/baseapi.h"
