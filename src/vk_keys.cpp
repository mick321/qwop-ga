#include "stdafx.h"
#include "vk_keys.h"

/// Inaccessible constructor.
PressKey::PressKey()
{
  for (int i = 0; i < 128; i++)
    mapVirtualToScan[kbdus[i]] = i;
}

/// Destructor. Releases all pushed keys.
PressKey::~PressKey()
{
  ReleaseAllPushed();
}

/// Push given virtual key.
void PressKey::Push(char key)
{
  if (instance.pushedKeys.count(key) || key == 0)
    return;

  INPUT Inp;
  ZeroMemory(&Inp, sizeof(INPUT));
  Inp.type = INPUT_KEYBOARD;
  Inp.ki.wVk = key;
  Inp.ki.wScan = instance.mapVirtualToScan[key];
  Inp.ki.time = 0;
  Inp.ki.dwFlags = 0;
  Inp.ki.dwExtraInfo = 0;
  SendInput(1, &Inp, sizeof(INPUT));
  instance.pushedKeys.insert(key);
}

/// Release given virtual key.
void PressKey::Release(char key, bool ignorePushedArray)
{
  if ((!ignorePushedArray && !instance.pushedKeys.count(key)) || key == 0)
    return;

  INPUT Inp;
  ZeroMemory(&Inp, sizeof(INPUT));
  Inp.type = INPUT_KEYBOARD;
  Inp.ki.wVk = key;
  Inp.ki.wScan = instance.mapVirtualToScan[key];
  Inp.ki.time = 0;
  Inp.ki.dwFlags = KEYEVENTF_KEYUP;
  Inp.ki.dwExtraInfo = 0;
  SendInput(1, &Inp, sizeof(INPUT));

  if (!ignorePushedArray)
    instance.pushedKeys.erase(key);
}

/// Pushes given keys and releases others. Keys are given through null terminated string.
void PressKey::PushOnly(const char* keys)
{
  int count = strlen(keys);

  for (auto& keyIt = instance.pushedKeys.begin(); keyIt != instance.pushedKeys.end(); keyIt++)
  {
    bool found = false;
    for (int i = 0; i < count; i++)
      if (keys[i] == *keyIt)
      {
        found = true; // we want to push it again, good, do not sent "release signal"
        break;
      }

    if (!found)
    {
      Release(*keyIt, true);
    }
  }

  instance.pushedKeys.clear(); // clear array so we repeat "push" signals

  for (int i = 0; i < count; i++)
  {
    Push(keys[i]);
  }
}

/// Release all pushed keys. Called when program finishes automatically.
void PressKey::ReleaseAllPushed()
{
  while (!instance.pushedKeys.empty())
  {
    Release(*(instance.pushedKeys.begin()));
  }
}

/// Click on given position.
void PressKey::Click(int x, int y)
{
  SetCursorPos(x, y);
  INPUT Inp;
  ZeroMemory(&Inp, sizeof(INPUT));
  Inp.type = INPUT_MOUSE;
  Inp.mi.dwFlags = MOUSEEVENTF_LEFTDOWN;
  SendInput(1, &Inp, sizeof(INPUT));
  Inp.mi.dwFlags = MOUSEEVENTF_LEFTUP;
  SendInput(1, &Inp, sizeof(INPUT));
}

// ------------------- static variables initialization -------------------

unsigned char PressKey::kbdus[128] =
{
    0,  27, '1', '2', '3', '4', '5', '6', '7', '8',	/* 9 */
  '9', '0', '-', '=', '\b',	/* Backspace */
  '\t',			/* Tab */
  'q', 'w', 'e', 'r',	/* 19 */
  't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\n',	/* Enter key */
    0,			/* 29   - Control */
  'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';',	/* 39 */
 '\'', '`',   0,		/* Left shift */
 '\\', 'z', 'x', 'c', 'v', 'b', 'n',			/* 49 */
  'm', ',', '.', '/',   0,				/* Right shift */
  '*',
    0,	/* Alt */
  ' ',	/* Space bar */
    0,	/* Caps lock */
    0,	/* 59 - F1 key ... > */
    0,   0,   0,   0,   0,   0,   0,   0,
    0,	/* < ... F10 */
    0,	/* 69 - Num lock*/
    0,	/* Scroll Lock */
    0,	/* Home key */
    0,	/* Up Arrow */
    0,	/* Page Up */
  '-',
    0,	/* Left Arrow */
    0,
    0,	/* Right Arrow */
  '+',
    0,	/* 79 - End key*/
    0,	/* Down Arrow */
    0,	/* Page Down */
    0,	/* Insert Key */
    0,	/* Delete Key */
    0,   0,   0,
    0,	/* F11 Key */
    0,	/* F12 Key */
    0,	/* All other keys are undefined */
};		

PressKey PressKey::instance;
