#pragma once 

// --------------------------------------------------------------------------------------------

// maximum count of genes
#define MAX_GENE_COUNT 100
// duration of one gene playback
#define GENE_DURATION  250
// default bits per gene (in our case: Q W O P = 4 bits of true/false).
#define GENE_BIT_COUNT 4

// --------------------------------------------------------------------------------------------

/// Entire chromosome of runner. 
/// NBITS says how many bits should be used for one gene, max is 8.
template <int NBITS>
class Chromosome
{
private:
  BYTE  chromosomeData[MAX_GENE_COUNT];   //!< coded chromosome
  int   geneCount;      //!< count of stored genes

public:
  /// Constructor.
  inline Chromosome(const int geneCount)
  {
    if (geneCount > 0 && geneCount <= MAX_GENE_COUNT)
    {
      this->geneCount = geneCount;
      memset(chromosomeData, 0, sizeof(BYTE) * geneCount);
    }
    else
    {
      THROW_FATAL("Invalid gene count. Must be at least 1 and at most .");
    }
  }

  /// Get boolean value of gene[geneNumber](geneBit).
  inline bool GetGene(const int geneNumber, const int geneBit) const
  {
    return (chromosomeData[geneNumber] & (1 << geneBit)) != 0;
  }

  /// Set boolean value of gene[geneNumber](geneBit).
  inline void SetGene(const int geneNumber, const int geneBit, const bool value)
  {
    BYTE& g = chromosomeData[geneNumber];
    if (value)
      g = g | (1 << geneBit); // set "geneBit" bit to non-zero
    else
      g = g & (~(1 << geneBit)); // set "geneBit" bit to zero
  }

  /// Randomize all bits of gene[geneNumber].
  inline void RandomizeGene(const int geneNumber)
  {
    chromosomeData[geneNumber] = rand() & ((1 << NBITS) - 1);
  }

  /// Randomize whole chromosome.
  inline void Randomize()
  {
    for (int i = 0; i < geneCount; i++)
      RandomizeGene(i);
  }

  /// Mutate this chromosome with given probability.
  /// @param probability probability of one bit mutation
  inline void Mutate(const float probability)
  {
    int probInt = (int)(probability * (RAND_MAX + 1));
    for (int i = 0; i < geneCount; i++)
      for (int j = 0; j < NBITS; j++)
        if (rand() < probInt)
          SetGene(i, j, !GetGene(i, j));
  }

  /// Make this chromosome by crossing two given as parameters. It is ok to pass "*this" as parameter.
  inline void Crossover(const Chromosome<NBITS>& parentA, const Chromosome<NBITS>& parentB)
  {
    int crossOverPoint1 = randInt(0, geneCount / 2);
    int crossOverPoint2 = randInt(geneCount / 2, geneCount);
    
    if (rand() % 2 == 0)
    {
      // A B A
      for (int i = 0; i < crossOverPoint1; i++)
        chromosomeData[i] = parentA.chromosomeData[i];
      for (int i = crossOverPoint1; i < crossOverPoint2; i++)
        chromosomeData[i] = parentB.chromosomeData[i];
      for (int i = crossOverPoint2; i < geneCount; i++)
        chromosomeData[i] = parentA.chromosomeData[i];
    }
    else
    {
      // B A B
      for (int i = 0; i < crossOverPoint1; i++)
        chromosomeData[i] = parentB.chromosomeData[i];
      for (int i = crossOverPoint1; i < crossOverPoint2; i++)
        chromosomeData[i] = parentA.chromosomeData[i];
      for (int i = crossOverPoint2; i < geneCount; i++)
        chromosomeData[i] = parentB.chromosomeData[i];
    }
  }

  /// Print chromosome sequence (to stream). 
  /// @param bitCharacters each character is name for bit of same index
  /// @param bitEmpty what to print when bit is false
  inline bool Print(const char* bitCharacters, const char bitFalse = '-', FILE* stream = stdout)
  {
    char buffer[MAX_GENE_COUNT * (NBITS + 1) + 16];
    char* pbuffer = buffer;
    int bitCharactersLen = strlen(bitCharacters);
    if (bitCharactersLen != NBITS)
      return false;

    for (int i = 0; i < geneCount; i++)
    {
      if (i > 0)
      {
        *pbuffer = '|';
        pbuffer++;
      }

      for (int j = 0; j < NBITS; j++)
      {
        *pbuffer = GetGene(i, j) ? bitCharacters[j] : bitFalse;
        pbuffer++;
      }
    }
    *pbuffer = '\0';
    return fprintf(stream, "%d|%s", geneCount, buffer) >= 0;
  }

  /// Scan chromosome sequence (from stream). 
  /// @param bitCharacters each character is name for bit of same index
  /// @param bitEmpty what is expected to scan when bit is false
  inline bool Scan(const char* bitCharacters, const char bitFalse = '-', FILE* stream = stdin)
  {
    char buffer[MAX_GENE_COUNT * (NBITS + 1) + 128];

    int geneCountToBeLoaded;
    char format[16];
    sprintf(format, "%%d|%%%ds", (NBITS + 1) * geneCount - 1);
    if (fscanf(stream, format, &geneCountToBeLoaded, buffer) != 2)
      return false;
    if (geneCountToBeLoaded != geneCount)
      return false;

    char* pbuffer = buffer;
    for (int i = 0; i < geneCount; i++)
    {
      if (i > 0)
      {
        if (*pbuffer != '|')
          return false;
        pbuffer++;
      }

      for (int j = 0; j < NBITS; j++)
      {
        if (*pbuffer == bitCharacters[j])
          SetGene(i, j, true);
        else if (*pbuffer == bitFalse)
          SetGene(i, j, false);
        else 
          return false;

        pbuffer++;
      }
    }
    return true;
  }
};