#pragma once

// --------------------------------------------------------------------------------------------

/// Fatal exception in execution.
class FatalException : public std::exception
{
private:
  char strbuffer[1024];
public:
  /// Constructor of fatal exception.
  inline FatalException(const char* msg, const char* sourceFile, const int sourceLine)
  {
    sprintf(strbuffer, "Exception in %.900s on line %d: %.50s", sourceFile, sourceLine, msg);
  }
  /// What happened?
  inline const char* what() const throw() { return strbuffer; }
};

#define THROW_FATAL(msg) throw FatalException((msg), __FILE__, __LINE__);

// --------------------------------------------------------------------------------------------

/// Enumeration of runner types.
enum RunnerType
{
  runnerUnknown = 0,
  runnerBasic,
  runnerAdvanced
};

// --------------------------------------------------------------------------------------------

/// Settings singleton.
class Settings
{
private:
  static Settings instance;

  /// Inaccessible default constructor.
  inline Settings() { AutoLoad(); }
  /// Inaccessible copy constructor.
  inline Settings(Settings& copyFrom) { }
  /// Inaccessible destructor.
  inline ~Settings() { AutoSave(); }

  /// Load some changeble values from configuration file.
  void AutoLoad();
  /// Save some changeble values to configuration file.
  void AutoSave();
public:
  /// Get The only possible instance.
  static inline Settings& GetInstance() { return instance; }

  RunnerType runnerType = runnerBasic; //!< used runner type
  int     runnerCount = 20;            //!< population size
  int     geneCount = 12;              //!< count of genes in chromosome (key sequence length) - 12 * 4bits
  float   mutationProbability = 0.05f; //!< probability of mutation
  int     eliteSurviveCount = 4;       //!< number of elites to survive to next generation
  int     timeBetweenKeySwitch = 250;  //!< time between switching to next key
  int     timeBetweenKeyHits = 50;    //!< time until pressing key should be repeated
  int     timeMax = 10*1000;           //!< maximum time to run (no need to run for eternity)
  float   bestWillFightProb = 0.333f;  //!< probability of best entering round
  
  int     captureX = 0, captureY = 0;  //!< QWOP game window top-left corner coordinates
  int     captureOffsetX = 160, captureOffsetY = 20;  //!< QWOP capture offset (we do not capture whole window)
  int     captureW = 320, captureH = 280;  //!< QWOP capture bitmap size

  unsigned char headColorThreshR = 25, headColorThreshG = 20, headColorThreshB = 10; //!< head recognition - maximum brightness of color channels

  unsigned char bodyColorThreshLowR = 145, bodyColorThreshG = 20, bodyColorThreshB = 35; //!< body recognition - brightness of color channels
  int     bodyStartX_OffsetFromHead = -100, bodyStartY_OffsetFromHead = 40;  //!< where to start searching body
  int     bodyEndX_OffsetFromHead = 100, bodyEndY_OffsetFromHead = 130;  //!< where to stop searching body
  
  float   headMidThresholdX = 0.175f;        //!< zone defining "mid" position of head (head is in mod zone if |headX - bodyX| <= headMidThresholdX)

  int     scoreOffsetX = 0, scoreOffsetY = 0, scoreW = 320, scoreH = 40; //!< score - capturing rectangle 
  int     gameOverX = 0, gameOverY = 100; //!< we detect fall as yellow pixel on certain position (there's a medal :) )

  int     refreshButtonX = 0, refreshButtonY = 0; //!< refresh button position
  int     refreshPeriod = 5;           //!< game consistently slows down (which I didn't know before), so we refresh page every N runs

  bool    debug = false;               //!< show debugging info
  bool    replayMode = false;          //!< if true, program will not learn, just play runners from replay file
};

// --------------------------------------------------------------------------------------------

/// Game state representation. What we know about runner.
struct GameState
{
  int    timestamp;      //!< time stamp of this state (ms since start of run)
  float  headX, headY;   //!< head position
  float  bodyX, bodyY;   //!< head position
  float  score;          //!< meters so far
  bool   gameOver;       //!< true if runner fell down or finished
  bool   valid;          //!< is this state valid?

  inline GameState() : timestamp(0), headX(0), headY(0), bodyX(0), bodyY(0), score(0), gameOver(false), valid(false) { }
};

// --------------------------------------------------------------------------------------------

/// Dummy marker implemented by window. Can be placed anywhere on screen
class DummyMarker
{
private:
  volatile HWND hWnd;
  volatile HWND wndLabel;
  volatile static LONG DummyMarkerCount;

  volatile bool finished = false;

  HANDLE threadHandle;
  char label[1024];

  /// Thread will marker's message queue.
  static DWORD WINAPI MsgAsyncProc(LPVOID userData);
  /// Window message processing.
  static LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
public:
  /// Constructor.
  DummyMarker(const char* title);
  /// Destructor.
  ~DummyMarker();

  /// Set position of marker (its top-left corner).
  void SetPosition(int x, int y);
  /// Set size of marker.
  void SetSize(int w, int h);
  /// Set label.
  void SetLabel(const char* label);
};

// --------------------------------------------------------------------------------------------

/// Convert boolean key flags to one string.
inline void KeysToStr(const bool keyQ, const bool keyW, const bool keyO, const bool keyP, char(&str)[5])
{
  int keysI = 0;
  if (keyQ)
    str[keysI++] = 'Q';
  if (keyW)
    str[keysI++] = 'W';
  if (keyO)
    str[keysI++] = 'O';
  if (keyP)
    str[keysI++] = 'P';

  str[keysI] = '\0';
}

//! Get random int from interval <from, to).
inline int randInt(int from, int to)
{
  if (to - from <= 0)
    return from;

  return from + rand() % (to - from);
}