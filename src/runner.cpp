#include "stdafx.h"
#include "runner.h"
#include "vk_keys.h"

/// Return new runner.
Runner* Runner::New(int id)
{
  switch (Settings::GetInstance().runnerType)
  {
  case runnerBasic:
    return new BasicRunner(id);
  case runnerAdvanced:
    return new AdvancedRunner(id);
  default:
    THROW_FATAL("Unknown runner type!");
  }
}

// --------------------------------------------------------------------------------------------

/// Constructor, parameter is instance id.
BasicRunner::BasicRunner(int id) : Runner(id), chromosome(Settings::GetInstance().geneCount), currentSeqIndex(0)
{ 
}

/// Reset inner state (does not erase chromosomes, just statistics). Should be called when runner starts to run.
void BasicRunner::Reset(const GameState& state)
{
  lastTimestamp = state.timestamp;
  headHeightSum = 0.f;
  totalScore = 0.f;
}

/// Update inner state and return 
void BasicRunner::Update(const GameState& state)
{
  auto& settings = Settings::GetInstance();
  currentSeqIndex = (state.timestamp / settings.timeBetweenKeySwitch) % settings.geneCount;
  
  headHeightSum += 0.001f * (state.timestamp - lastTimestamp) * (1.f - state.headY);
  lastTimestamp = state.timestamp;
  totalScore = state.score;
}

/// Obtain keys that should be pressed.
void BasicRunner::GetKeys(bool& pressQ, bool& pressW, bool& pressO, bool& pressP)
{
  pressQ = chromosome.GetGene(currentSeqIndex, 0);
  pressW = chromosome.GetGene(currentSeqIndex, 1);
  pressO = chromosome.GetGene(currentSeqIndex, 2);
  pressP = chromosome.GetGene(currentSeqIndex, 3);
}

/// Get fitness value.
float BasicRunner::GetFitness()
{
  float percentOfTime = ((float)min(Settings::GetInstance().timeMax, lastTimestamp) / Settings::GetInstance().timeMax);
  float fitness = max(0, totalScore) * max(0, totalScore) + percentOfTime;

  cachedFitness = fitness;
  cachedFitnessValid = true;
  return fitness;
}

/// Rewrite this runner by child of two given parents.
void BasicRunner::CrossoverAndMutate(const Runner& parentA, const Runner& parentB)
{
  if (!SameTypeAs(parentA) && !SameTypeAs(parentB))
    THROW_FATAL("Cannot work with different types of runners");

  chromosome.Crossover(
    static_cast<const BasicRunner&>(parentA).chromosome,
    static_cast<const BasicRunner&>(parentB).chromosome);
  chromosome.Mutate(Settings::GetInstance().mutationProbability);
}

/// Randomize this runner (rewrite its chromosomes by random data).
void BasicRunner::Randomize()
{
  chromosome.Randomize();
}

/// Print this runner to stream.
bool BasicRunner::Print(FILE* stream)
{
  if (fprintf(stream, "basic;id;%d;hasfitness;%d;fitness;%g;distance;%g;time;%d;headHeight;%g;",
    id,
    cachedFitnessValid ? 1 : 0,
    cachedFitnessValid ? cachedFitness : 0,
    cachedFitnessValid ? totalScore : 0,
    cachedFitnessValid ? lastTimestamp : 0,
    cachedFitnessValid ? headHeightSum * 1000.f / lastTimestamp : 0) < 0)
    return false;

  if (!chromosome.Print("QWOP", '-', stream))
    return false;

  if (fprintf(stream, ";\n") < 0)
    return false;

  return true;
}

/// Scan this runner from stream.
bool BasicRunner::Scan(FILE* stream)
{
  char tempstr[24];
  int hasFitnessInt;
  float tempfloat;
  if (fscanf(stream, "basic;id;%d;hasfitness;%d;fitness;%f;distance;%f;time;%d;headHeight;%g;",
    &id,
    &hasFitnessInt,
    &cachedFitness,
    &totalScore,
    &lastTimestamp,
    &tempfloat) != 6)
    return false;

  cachedFitnessValid = (hasFitnessInt != 0);
  headHeightSum = tempfloat * lastTimestamp / 1000.f;

  if (!chromosome.Scan("QWOP", '-', stream))
    return false;

  fscanf(stream, "%16s\n", tempstr);
  if (!strcmp(tempstr, ";"))
    return true;
  else
    return false;
}

// --------------------------------------------------------------------------------------------

/// Constructor, parameter is instance id.
AdvancedRunner::AdvancedRunner(int id)
  : Runner(id), 
  chromosomeHeadLeft(Settings::GetInstance().geneCount),
  chromosomeHeadRight(Settings::GetInstance().geneCount),
  //chromosomeHeadMid(Settings::GetInstance().geneCount),
  currentSeqIndex(0)
{
}

/// Reset inner state (does not erase chromosomes, just statistics). Should be called when runner starts to run.
void AdvancedRunner::Reset(const GameState& state)
{
  lastTimestamp = state.timestamp;
  headHeightSum = 0.f;
  totalScore = 0.f;
  fellToLeftRight = 0;
  lastHeadPosX = 0;
  lastBodyPosX = 0;
}

/// Update inner state and return 
void AdvancedRunner::Update(const GameState& state)
{
  auto& settings = Settings::GetInstance();
  int lastSeqIndex = currentSeqIndex;
  currentSeqIndex = (state.timestamp / settings.timeBetweenKeySwitch) % settings.geneCount;
  if (currentSeqIndex != lastSeqIndex)
  {
    /*
    if (lastHeadPosX >= lastBodyPosX - settings.headMidThresholdX && lastHeadPosX <= lastBodyPosX + settings.headMidThresholdX)
      currentProgramMode = 0;
    else*/ if (lastHeadPosX < lastBodyPosX)
      currentProgramMode = -1;
    else
      currentProgramMode = +1;
  }

  headHeightSum += 0.001f * (state.timestamp - lastTimestamp) * (1.f - state.headY);
  lastTimestamp = state.timestamp;
  totalScore = state.score;
  lastHeadPosX = state.headX;
  lastBodyPosX = state.bodyX;

  if (state.gameOver && fellToLeftRight == 0)
  {
    fellToLeftRight = (lastHeadPosX < lastBodyPosX) ? -1 : 1;
    if (fellToLeftRight == -1)
      printf("Fell on back.\n");
    else if (fellToLeftRight == 1)
      printf("Fell on forefront.\n");
  }
}

/// Obtain keys that should be pressed.
void AdvancedRunner::GetKeys(bool& pressQ, bool& pressW, bool& pressO, bool& pressP)
{
  auto& settings = Settings::GetInstance();
/*  if (currentProgramMode == 0)
  {
    pressQ = chromosomeHeadMid.GetGene(currentSeqIndex, 0);
    pressW = chromosomeHeadMid.GetGene(currentSeqIndex, 1);
    pressO = chromosomeHeadMid.GetGene(currentSeqIndex, 2);
    pressP = chromosomeHeadMid.GetGene(currentSeqIndex, 3);
    printf("\b\bM ");
  }
  else*/ if (currentProgramMode == -1)
  {
    pressQ = chromosomeHeadLeft.GetGene(currentSeqIndex, 0);
    pressW = chromosomeHeadLeft.GetGene(currentSeqIndex, 1);
    pressO = chromosomeHeadLeft.GetGene(currentSeqIndex, 2);
    pressP = chromosomeHeadLeft.GetGene(currentSeqIndex, 3);
    printf("\b\bL ");
  }
  else
  {
    pressQ = chromosomeHeadRight.GetGene(currentSeqIndex, 0);
    pressW = chromosomeHeadRight.GetGene(currentSeqIndex, 1);
    pressO = chromosomeHeadRight.GetGene(currentSeqIndex, 2);
    pressP = chromosomeHeadRight.GetGene(currentSeqIndex, 3);
    printf("\b\bR ");
  }
}

/// Get fitness value.
float AdvancedRunner::GetFitness()
{
  float percentOfTime = ((float)min(Settings::GetInstance().timeMax, lastTimestamp) / Settings::GetInstance().timeMax);
  float fitness = max(0, totalScore) * max(0, totalScore) + percentOfTime;

  cachedFitness = fitness;
  cachedFitnessValid = true;
  return fitness;
}

/// Rewrite this runner by child of two given parents.
void AdvancedRunner::CrossoverAndMutate(const Runner& parentA, const Runner& parentB)
{
  if (!SameTypeAs(parentA) && !SameTypeAs(parentB))
    THROW_FATAL("Cannot work with different types of runners");

  const auto& runnerA = static_cast<const AdvancedRunner&>(parentA);
  const auto& runnerB = static_cast<const AdvancedRunner&>(parentB);

  // *** LEFT ***
  chromosomeHeadLeft.Crossover(runnerA.chromosomeHeadLeft, runnerB.chromosomeHeadLeft);
  chromosomeHeadLeft.Mutate(Settings::GetInstance().mutationProbability);

  // *** RIGHT ***
  chromosomeHeadRight.Crossover(runnerA.chromosomeHeadRight, runnerB.chromosomeHeadRight);
  chromosomeHeadRight.Mutate(Settings::GetInstance().mutationProbability);

  //chromosomeHeadMid.Crossover(
  //  runnerA.chromosomeHeadMid,
  //  runnerB.chromosomeHeadMid);
  //chromosomeHeadMid.Mutate(Settings::GetInstance().mutationProbability);
}

/// Randomize this runner (rewrite its chromosomes by random data).
void AdvancedRunner::Randomize()
{
  chromosomeHeadLeft.Randomize();
  chromosomeHeadRight.Randomize();
  //chromosomeHeadMid.Randomize();
}

/// Print this runner to stream.
bool AdvancedRunner::Print(FILE* stream)
{
  if (fprintf(stream, "basic;id;%d;hasfitness;%d;fitness;%g;distance;%g;time;%d;headHeight;%g;fellTo;%d;",
    id,
    cachedFitnessValid ? 1 : 0,
    cachedFitnessValid ? cachedFitness : 0,
    cachedFitnessValid ? totalScore : 0,
    cachedFitnessValid ? lastTimestamp : 0,
    cachedFitnessValid ? headHeightSum * 1000.f / lastTimestamp : 0,
    cachedFitnessValid ? fellToLeftRight : 0) < 0)
    return false;

  if (!chromosomeHeadLeft.Print("QWOP", '-', stream))
    return false;
  if (fprintf(stream, ";") < 0)
    return false;

  if (!chromosomeHeadRight.Print("QWOP", '-', stream))
    return false;
  //if (fprintf(stream, ";") < 0)
  //  return false;

  //if (!chromosomeHeadMid.Print("QWOP", '-', stream))
  //  return false;
  if (fprintf(stream, ";\n") < 0)
    return false;

  return true;
}

/// Scan this runner from stream.
bool AdvancedRunner::Scan(FILE* stream)
{
  char tempstr[24];
  int hasFitnessInt;
  float tempfloat;
  if (fscanf(stream, "basic;id;%d;hasfitness;%d;fitness;%f;distance;%f;time;%d;headHeight;%g;fellTo;%d;",
    &id,
    &hasFitnessInt,
    &cachedFitness,
    &totalScore,
    &lastTimestamp,
    &tempfloat,
    &fellToLeftRight) != 7)
    return false;

  cachedFitnessValid = (hasFitnessInt != 0);
  headHeightSum = tempfloat * lastTimestamp / 1000.f;

  if (!chromosomeHeadLeft.Scan("QWOP", '-', stream))
    return false;

  fscanf(stream, "%c", &tempstr[0]);
  if (tempstr[0] != ';')
    return false;

  if (!chromosomeHeadRight.Scan("QWOP", '-', stream))
    return false;

  //fscanf(stream, "%c", &tempstr[0]);
  //if (tempstr[0] != ';')
  //  return false;

  //if (!chromosomeHeadMid.Scan("QWOP", '-', stream))
  //  return false;

  fscanf(stream, "%16s\n", tempstr);
  if (!strcmp(tempstr, ";"))
    return true;
  else
    return false;
}