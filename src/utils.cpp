#include "stdafx.h"
#include "utils.h"

// -----------------------------------------------------------------------------------

/// Load some changeble values from configuration file.
void Settings::AutoLoad()
{
  FILE* f = fopen("settings.cfg", "r");
  if (!f)
    return;

  char what[1024];
  char value[1024];

  while (1 == fscanf(f, "%1024s", what))
  {
    if (!strcmp(what, "mode"))
    {
      fscanf(f, "%1024s", value);
      if (!strcmp(value, "learn"))
        replayMode = false;
      else if (!strcmp(value, "replay"))
        replayMode = true;
    }
    else if (!strcmp(what, "runnertype"))
    {
      fscanf(f, "%1024s", value);
      if (!strcmp(value, "basic"))
        runnerType = runnerBasic;
      else if (!strcmp(value, "advanced"))
        runnerType = runnerAdvanced;
    }
    else
    {
      fprintf(stderr, "Unrecognized token in settings file.\n");
      break; // unrecognized token, we cannot continue
    }
  }

  fclose(f);
}

/// Save some changeble values to configuration file.
void Settings::AutoSave()
{
  FILE* f = fopen("settings.cfg", "w");
  if (!f)
    return;

  fprintf(f, "mode %s\n", replayMode ? "replay" : "learn");
  fprintf(f, "runnertype %s\n", runnerType == runnerBasic ? "basic" : runnerType == runnerAdvanced ? "advanced" : "unknown");

  fclose(f);
}

// -----------------------------------------------------------------------------------

volatile LONG DummyMarker::DummyMarkerCount = 0;

/// Constructor.
DummyMarker::DummyMarker(const char* title)
{
  strcpy(label, title);
  threadHandle = CreateThread(NULL, 0, DummyMarker::MsgAsyncProc, this, 0, NULL);
}

/// Destructor.
DummyMarker::~DummyMarker()
{
  finished = true;

  if (threadHandle)
  {
    WaitForSingleObject(threadHandle, INFINITE);
    CloseHandle(threadHandle);
    threadHandle = NULL;
  }
}

/// Set position of marker (its top-left corner).
void DummyMarker::SetPosition(int x, int y)
{
  if (hWnd)
    SetWindowPos(hWnd, NULL, x, y, 0, 0, SWP_NOSIZE | SWP_NOREPOSITION);
}

/// Set size of marker.
void DummyMarker::SetSize(int w, int h)
{
  if (hWnd)
    SetWindowPos(hWnd, NULL, 0, 0, w, h, SWP_NOMOVE | SWP_NOREPOSITION);
}

/// Set label.
void DummyMarker::SetLabel(const char* label)
{
  strcpy(this->label, label);
  SetWindowText(wndLabel, this->label);
  InvalidateRect(hWnd, NULL, true);
}

/// Thread will marker's message queue.
DWORD WINAPI DummyMarker::MsgAsyncProc(LPVOID userData)
{
  DummyMarker* pTHIS = reinterpret_cast<DummyMarker*>(userData);

  // register window class
  LONG wndNumber = InterlockedIncrement(&DummyMarker::DummyMarkerCount);
  //if (wndNumber == 1)
  {
    WNDCLASSEX wndClass;
    ZeroMemory(&wndClass, sizeof(WNDCLASSEX));
    wndClass.cbSize = sizeof(WNDCLASSEX);
    wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_NOCLOSE;
    wndClass.lpfnWndProc = WndProc;
    wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndClass.hInstance = GetModuleHandle(NULL);
    wndClass.hbrBackground = GetSysColorBrush(COLOR_WINDOW);
    wndClass.lpszClassName = "QWOPDummyMarker";
    RegisterClassEx(&wndClass);
  }

  // create window
  pTHIS->hWnd = CreateWindowEx(WS_EX_TOPMOST | WS_EX_TOOLWINDOW, "QWOPDummyMarker", pTHIS->label, 
    WS_POPUP | WS_VISIBLE, 0, 0, 100, 24, NULL, NULL, NULL, 0);
  
  // create label
  pTHIS->wndLabel = CreateWindow("static", "label",
    WS_CHILD | WS_VISIBLE | WS_TABSTOP,
    3, 3, 100, 24,
    pTHIS->hWnd, NULL,
    (HINSTANCE)GetWindowLong(pTHIS->hWnd, GWL_HINSTANCE), NULL);
  SetWindowText(pTHIS->wndLabel, pTHIS->label);

  // message loop (until WM_QUIT)
  MSG msg;
  while (!pTHIS->finished)
  {
    while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
    {
      TranslateMessage(&msg);
      DispatchMessage(&msg);
    }
    Sleep(10);
  }

  // destroy window (label destroyed automatically)
  DestroyWindow(pTHIS->hWnd);
  pTHIS->hWnd = NULL;
  
  // unregister class
  wndNumber = InterlockedDecrement(&DummyMarker::DummyMarkerCount);
  if (wndNumber == 0)
  {
    UnregisterClass("QWOPDummyMarker", GetModuleHandle(NULL));
  }

  return 0;
}

/// Window message processing.
LRESULT CALLBACK DummyMarker::WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
  switch (uMsg)
  {
  case WM_CTLCOLORSTATIC:
    {
      SetTextColor((HDC)wParam, RGB(150, 150, 150));
      return (LRESULT)GetStockObject(HOLLOW_BRUSH);
    }
  case WM_QUIT:
    return 0;
  default:;
  }
  return CallWindowProc(DefWindowProc, hwnd, uMsg, wParam, lParam);
}

// --------------- static variables ------------------------

Settings Settings::instance;