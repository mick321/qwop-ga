#pragma once

class PressKey
{
private:
  /// KBDUS means US Keyboard Layout. This is a scancode table
  /// used to layout a standard US keyboard.
  static unsigned char kbdus[128];
  /// Singleton instance.
  static PressKey instance;

  std::map<int, int> mapVirtualToScan; //!< virtual key to scan code
  std::set<char> pushedKeys;           //!< keys that are currently pressed

  /// Inaccessible copy constructor.
  inline PressKey(PressKey& copyFrom) { }
  /// Inaccessible constructor.
  PressKey();
public:
  /// Destructor. Releases all pushed keys.
  ~PressKey();

  /// Push given virtual key.
  static void Push(char key);
  /// Release given virtual key.
  static void Release(char key, bool ignorePushedArray = false);
  /// Pushes given keys and releases others. Keys are given through null terminated string.
  static void PushOnly(const char* keys);
  /// Release all pushed keys. Called when program finishes automatically.
  static void ReleaseAllPushed();

  /// Click on given position.
  static void Click(int x, int y);
};

extern void stiskni(WORD key);

