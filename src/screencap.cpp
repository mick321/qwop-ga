#include "stdafx.h"
#include "screencap.h"
#include "utils.h"

// ------------------------------------------------------------------------

/// Constructor. Initializes all necessary objects.
ScreenCaptureLoop::ScreenCaptureLoop()
{
  const auto& settings = Settings::GetInstance();
  initOk = false;

  if (tessAPI.Init(NULL, NULL) != 0)
  {
    THROW_FATAL("Error: Cannot initialize tesseract library.");
  }

  // Retrieve the handle to a display device context for the client area of the window. 
  hdcScreen = GetDC(NULL);
  if (!hdcScreen)
    return;

  // Create a compatible DC which is used in a BitBlt from the window DC
  hdcMemDC = CreateCompatibleDC(hdcScreen);
  if (!hdcMemDC)
    return;

  // Create a compatible bitmap from the Window DC
  hbmScreen = CreateCompatibleBitmap(hdcScreen, settings.captureW, settings.captureH);
  if (!hbmScreen)
    return;

  // Select the compatible bitmap into the compatible memory DC.
  SelectObject(hdcMemDC, hbmScreen);

  // Get the BITMAP from the HBITMAP
  GetObject(hbmScreen, sizeof(BITMAP), &bmpScreen);

  bi.biSize = sizeof(BITMAPINFOHEADER);
  bi.biWidth = bmpScreen.bmWidth;
  bi.biHeight = bmpScreen.bmHeight;
  bi.biPlanes = 1;
  bi.biBitCount = 32;
  bi.biCompression = BI_RGB;
  bi.biSizeImage = 0;
  bi.biXPelsPerMeter = 0;
  bi.biYPelsPerMeter = 0;
  bi.biClrUsed = 0;
  bi.biClrImportant = 0;

  DWORD dwBmpSize = ((bmpScreen.bmWidth * bi.biBitCount + 31) / 32) * 4 * bmpScreen.bmHeight;

  // finally, local picture data
  hDIB = new unsigned char[dwBmpSize];
  if (!hDIB)
    return;

  hDIB_Text = new unsigned char[settings.scoreW * settings.scoreH];
  if (!hDIB_Text)
    return;

  if (Settings::GetInstance().debug)
  {
    debugMarkHeadPos = new DummyMarker("<-head"); 
    debugMarkBodyPos = new DummyMarker("<-body");
    debugMarkScore = new DummyMarker("score");
  }

  initOk = true;
}

/// Destructor. Waits for the thread to finish.
ScreenCaptureLoop::~ScreenCaptureLoop()
{
  threadRunning = false;
  if (threadHandle)
  {
    WaitForSingleObject(threadHandle, INFINITE);
    CloseHandle(threadHandle);
    threadHandle = NULL;
  }

  if (debugMarkHeadPos)
  {
    delete debugMarkHeadPos;
    debugMarkHeadPos = NULL;
  }
  if (debugMarkBodyPos)
  {
    delete debugMarkBodyPos;
    debugMarkBodyPos = NULL;
  }
  if (debugMarkScore)
  {
    delete debugMarkScore;
    debugMarkScore = NULL;
  }

  if (hDIB)
  {
    delete[] hDIB;
    hDIB = NULL;
  }

  if (hDIB_Text)
  {
    delete[] hDIB_Text;
    hDIB_Text = NULL;
  }

  if (hbmScreen)
  {
    DeleteObject(hbmScreen);
    hbmScreen = NULL;
  }

  if (hdcMemDC)
  {
    ReleaseDC(NULL, hdcMemDC);
    hdcMemDC = NULL;
  }

  if (hdcScreen)
  {
    ReleaseDC(NULL, hdcScreen);
    hdcScreen = NULL;
  }
}

/// Capture screen area.
void ScreenCaptureLoop::CaptureScreen()
{
  // Bit block transfer into our compatible memory DC.
  if (!BitBlt(hdcMemDC,
    0, 0,
    Settings::GetInstance().captureW, Settings::GetInstance().captureH,
    hdcScreen,
    Settings::GetInstance().captureX, Settings::GetInstance().captureY,
    SRCCOPY | CAPTUREBLT))
  {
    fprintf(stderr, "Warning: BitBlt has failed.\n");
    return;
  }

  // Gets the "bits" from the bitmap and copies them into a buffer 
  // which is pointed to by lpbitmap.
  GetDIBits(hdcScreen, hbmScreen, 0,
    (UINT)bmpScreen.bmHeight,
    hDIB,
    (BITMAPINFO *)&bi, DIB_RGB_COLORS);
}

/// Start the thread. 
bool ScreenCaptureLoop::Start()
{
  if (initOk)
  {
    threadHandle = CreateThread(NULL, 0, ScreenCaptureLoop::ThreadProcStatic, this, 0, NULL);
    return (threadHandle != NULL);
  }

  return false;
}

/// Get current game state.
GameState ScreenCaptureLoop::GetGameState()
{
  LockGameState();
  GameState rtn = gameState;
  UnlockGameState();
  return rtn;
}

/// Thread entry point.
DWORD WINAPI ScreenCaptureLoop::ThreadProcStatic(_In_  LPVOID lpParameter)
{
  return reinterpret_cast<ScreenCaptureLoop*>(lpParameter)->ThreadProc();
}

/// Thread entry point (with context of "this").
DWORD WINAPI ScreenCaptureLoop::ThreadProc()
{
  startTime = GetTickCount();
  while (threadRunning)
  {
    // at first, we capture picture from screen
    CaptureScreen();

    GameState internalGameState;
    internalGameState.valid = true;
    internalGameState.timestamp = (int)(GetTickCount() - startTime);

    // recognize head position
    float headX, headY;
    if (FindHeadInBitmap(headX, headY))
    {
      internalGameState.headX = headX; // success, head found
      internalGameState.headY = headY;

      float bodyX, bodyY;
      // we know, where the head is -> find body
      if (FindBodyInBitmap(headX, headY, bodyX, bodyY))
      {
        internalGameState.bodyX = bodyX; // success, body found
        internalGameState.bodyY = bodyY;
      }
      else if(gameState.valid) // failure, try to copy from last game state
      {
        internalGameState.bodyX = gameState.bodyX;
        internalGameState.bodyY = gameState.bodyY;
      }
      else
      {
        internalGameState.valid = false;
      }
    }
    else if (gameState.valid)
    { 
      // success, head not found, copy from last game state
      internalGameState.headX = gameState.headX;
      internalGameState.headY = gameState.headY;
    }
    else
    {
      internalGameState.valid = false;
    }

    // recognize score
    float score;
    if (ReadScoreFromBitmap(score))
    {
      internalGameState.score = score;
    }
    else if (gameState.valid)
    {
      internalGameState.score = gameState.score;
    }
    else
    {
      internalGameState.valid = false;
    }

    // write game state it to shared memory
    internalGameState.gameOver = DetectGameOver();
    LockGameState();
    if (!nextRunner)
    {
      gameState = internalGameState;
    }
    else
    {
      nextRunner = false; // ah! after all I've done, I must throw my results away... :(
    }
    UnlockGameState();
  }
  return 0;
}

/// Find head position in bitmap. Given coordinates are normalized x:(-aspect, aspect), y:(-1, 1).
/// Returns false if not found.
bool ScreenCaptureLoop::FindHeadInBitmap(float& x, float& y)
{
  const auto& settings = Settings::GetInstance();
  const int halfSizeX = settings.captureW / 2;
  const int halfSizeY = settings.captureH / 2;

  int headKernelX = -1, headKernelY = -1;

  for (int j = 0; j < settings.captureH; j += 3)
  {
    for (int i = 0; i < settings.captureW; i += 3)
    {
      unsigned char* pixel = &hDIB[((settings.captureH - 1 - j)*settings.captureW + i) * 4];
      if (pixel[0] < settings.headColorThreshB && pixel[1] < settings.headColorThreshG && pixel[2] < settings.headColorThreshR)
      {
        headKernelX = i;
        headKernelY = j;
        goto FindHeadInBitmap_break_from_nested_loop;
      }
    }
  }
FindHeadInBitmap_break_from_nested_loop:

  if (headKernelX == -1 || headKernelY == -1)
    return false;

  int searchLeft = max(0, headKernelX - 20);
  int searchTop = max(0, headKernelY - 10);
  int searchRight = min(settings.captureW - 1, headKernelX + 20);
  int searchBottom = min(settings.captureH - 1, headKernelY + 30);

  int avgX = 0;
  int avgY = 0;
  int count = 0;

  for (int j = searchTop; j <= searchBottom; j++)
    for (int i = searchLeft; i <= searchRight; i++)
    {
      unsigned char* pixel = &hDIB[((settings.captureH - 1 - j)*settings.captureW + i) * 4];
      if (pixel[0] < settings.headColorThreshB && pixel[1] < settings.headColorThreshG && pixel[2] < settings.headColorThreshR)
      {
        avgX += i;
        avgY += j;
        count++;
      }
    }
  avgX /= count;
  avgY /= count;

  x = (float)(avgX - halfSizeX) / halfSizeY; // yes, divide by Y size (do not destroy aspect)
  y = (float)(avgY - halfSizeY) / halfSizeY;

  if (settings.debug)
  {
    debugMarkHeadPos->SetPosition(avgX + settings.captureX + 40, avgY + settings.captureY - 10);
  }

  return true;
}

/// Find body position in bitmap. Given coordinates are normalized x:(-aspect, aspect), y:(-1, 1).
/// Returns false if not found.
bool ScreenCaptureLoop::FindBodyInBitmap(float headPosX, float headPosY, float& x, float& y)
{
  const auto& settings = Settings::GetInstance();
  const int halfSizeX = settings.captureW / 2;
  const int halfSizeY = settings.captureH / 2;
  int headPxX = (int)((headPosX * halfSizeY) + halfSizeX); // yes, multiply by Y size (do not destroy aspect ratio)
  int headPxY = (int)((headPosY * halfSizeY) + halfSizeY);

  int searchLeft = max(0, headPxX + settings.bodyStartX_OffsetFromHead);
  int searchTop = max(0, headPxY + settings.bodyStartY_OffsetFromHead);
  int searchRight = min(settings.captureW - 1, headPxX + settings.bodyEndX_OffsetFromHead);
  int searchBottom = min(settings.captureH - 1, headPxY + settings.bodyEndY_OffsetFromHead);

  int avgX = 0;
  int avgY = 0;
  int count = 0;
  for (int j = searchTop; j <= searchBottom; j++)
    for (int i = searchLeft; i <= searchRight; i++)
    {
      unsigned char* pixel = &hDIB[((settings.captureH - 1 - j)*settings.captureW + i) * 4];
      if (pixel[0] < settings.bodyColorThreshB && pixel[1] < settings.bodyColorThreshG && pixel[2] > settings.bodyColorThreshLowR)
      {
        avgX += i;
        avgY += j;
        count++;
      }
    }

  if (count > 0)
  {
    avgX /= count;
    avgY /= count;

    x = (float)(avgX - halfSizeX) / halfSizeY; // yes, divide by Y size (do not destroy aspect)
    y = (float)(avgY - halfSizeY) / halfSizeY;

    if (settings.debug)
    {
      debugMarkBodyPos->SetPosition(avgX + settings.captureX + 40, avgY + settings.captureY - 10);
    }

    return true;
  }
  return false;
}

/// Read score from bitmap.
/// Returns false if unreadable.
bool ScreenCaptureLoop::ReadScoreFromBitmap(float& score)
{
  // copy part of image and apply threshold
  const auto& settings = Settings::GetInstance();
  unsigned char* pDIB_Text = hDIB_Text;
  const int yStart = settings.captureH - 1 - settings.scoreOffsetY;
  for (int j = yStart; j > yStart - settings.scoreH; j--)
    for (int i = settings.scoreOffsetX; i < settings.scoreOffsetX + settings.scoreW; i++)
    {
      *pDIB_Text = hDIB[4 * (j * settings.captureW + i) + 1] > 200 ? 255 : 0;
      pDIB_Text++;
    }

  // recognize text in bitmap
  char* text = tessAPI.TesseractRect(hDIB_Text, 1, settings.scoreW, 0, 0, settings.scoreW, settings.scoreH);
  float scoreRecognized;
  char checkstr[32];

  // gather number from recognized text
  bool success;
  int numRead = sscanf(text, "%f%30s", &scoreRecognized, checkstr);
  if (numRead == 2 && !strcmp(checkstr, "metres"))
  {
    score = scoreRecognized;
    if (settings.debug)
    {
      debugMarkScore->SetPosition(settings.captureX + settings.scoreOffsetX + settings.scoreW, settings.captureY + settings.scoreOffsetY);
      debugMarkScore->SetLabel(text);
    }
    success = true;
  }
  else
  {
    if (settings.debug)
    {
      debugMarkScore->SetPosition(settings.captureX + settings.scoreOffsetX + settings.scoreW, settings.captureY + settings.scoreOffsetY);
      debugMarkScore->SetLabel("unrecognized");
    }
    success = false;
  }

  delete[] text;
  return success;
}

/// Returns true if game is over.
bool ScreenCaptureLoop::DetectGameOver()
{
  const auto& settings = Settings::GetInstance();
  const unsigned char* pixel = &hDIB[4 * ((settings.captureH - 1 - settings.gameOverY) * settings.captureW + settings.gameOverX)];
  if (pixel[0] < 10 && 
    // pixel[1] > 245 &&  // well, calibration might be a "bit" off, red pixel or yellow, both are good.
    pixel[2] > 245)
  {
    if (settings.debug)
    {
      debugMarkScore->SetLabel("game over");
    }
    return true;
  }
  else
    return false;
}