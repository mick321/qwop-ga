#pragma once

#include "utils.h"
#include "chromosome.h"

// --------------------------------------------------------------------------------------------

/// Abstract runner class.
class Runner
{
protected:
  const int id;
  float cachedFitness = 0.f;    //!< we will store fitness function (run once, read result many times)
  bool  cachedFitnessValid = false;

  /// Constructor, parameter is instance id.
  inline Runner(int id) : id(id) { }
public:

  /// Get runner unique id.
  inline int GetId() const { return id; }

  /// Checking type of runner.
  inline bool SameTypeAs(const Runner& runner) { return GetRunnerTypeId() == runner.GetRunnerTypeId(); }
  /// Get runner type id. Each type of runner must have different ID.
  virtual RunnerType GetRunnerTypeId() const { return runnerUnknown; }

  /// Reset inner state (does not erase chromosomes, just statistics). Should be called when runner starts to run.
  virtual void Reset(const GameState& state) = 0;
  /// Update inner state.
  virtual void Update(const GameState& state) = 0;
  /// Obtain keys that should be pressed.
  virtual void GetKeys(bool& pressQ, bool& pressW, bool& pressO, bool& pressP) = 0;

  /// Obtain cached fitness function, returns false if there is no cached fitness.
  inline bool GetCachedFitness(float& fitness) const { if (cachedFitnessValid) { fitness = cachedFitness; } return cachedFitnessValid; }
  /// Set cached fitness function. Use carefully.
  inline void SetCachedFitness(float fitness) { cachedFitnessValid = true; cachedFitness = fitness; }

  /// Get fitness value. Larger is better.
  virtual float GetFitness() = 0;
  /// Rewrite this runner by child of two given parents.
  virtual void CrossoverAndMutate(const Runner& parentA, const Runner& parentB) = 0;
  /// Randomize this runner (rewrite its chromosomes by random data).
  virtual void Randomize() = 0;

  /// Print this runner to stream.
  virtual bool Print(FILE* stream = stdout) = 0;
  /// Scan this runner from stream.
  virtual bool Scan(FILE* stream = stdin) = 0;

  /// Return new runner.
  static Runner* New(int id);
};

// --------------------------------------------------------------------------------------------

/// Basic runner with one chromosome.
class BasicRunner : public Runner
{
private:
  friend class Runner;
  Chromosome<4> chromosome;
  int currentSeqIndex;

  int lastTimestamp = 0;
  float headHeightSum = 0;
  float totalScore = 0;

  /// Constructor, parameter is instance id.
  BasicRunner(int id);
public:

  /// Get runner type id. Each type of runner must have different ID.
  virtual RunnerType GetRunnerTypeId() const { return runnerBasic; }

  /// Reset inner state (does not erase chromosomes, just statistics). Should be called when runner starts to run.
  virtual void Reset(const GameState& state) override;
  /// Update inner state and return 
  virtual void Update(const GameState& state) override;
  /// Obtain keys that should be pressed.
  virtual void GetKeys(bool& pressQ, bool& pressW, bool& pressO, bool& pressP) override;

  /// Get fitness value. Larger is better.
  virtual float GetFitness() override;
  /// Rewrite this runner by child of two given parents.
  virtual void CrossoverAndMutate(const Runner& parentA, const Runner& parentB) override;
  /// Randomize this runner (rewrite its chromosomes by random data).
  virtual void Randomize() override;

  /// Print this runner to stream.
  virtual bool Print(FILE* stream = stdout) override;
  /// Scan this runner from stream.
  virtual bool Scan(FILE* stream = stdin) override;
};

// --------------------------------------------------------------------------------------------

/// Advanced runner with two chromosome => mixing two behaviors .
class AdvancedRunner : public Runner
{
private:
  friend class Runner;
  Chromosome<4> chromosomeHeadLeft, chromosomeHeadRight;
  int currentSeqIndex;

  int lastTimestamp = 0;
  float lastHeadPosX = 0;
  float lastBodyPosX = 0;
  float headHeightSum = 0;
  float totalScore = 0;

  int currentProgramMode = 0; //!< -1 if runner leans to left, +1 if to right, 0 represents mid

  int fellToLeftRight = 0; //!< -1 if runner fell to left (on back), +1 if to right (on forefront)

  /// Constructor, parameter is instance id.
  AdvancedRunner(int id);
public:

  /// Get runner type id. Each type of runner must have different ID.
  virtual RunnerType GetRunnerTypeId() const { return runnerBasic; }

  /// Reset inner state (does not erase chromosomes, just statistics). Should be called when runner starts to run.
  virtual void Reset(const GameState& state) override;
  /// Update inner state and return 
  virtual void Update(const GameState& state) override;
  /// Obtain keys that should be pressed.
  virtual void GetKeys(bool& pressQ, bool& pressW, bool& pressO, bool& pressP) override;

  /// Get fitness value. Larger is better.
  virtual float GetFitness() override;
  /// Rewrite this runner by child of two given parents.
  virtual void CrossoverAndMutate(const Runner& parentA, const Runner& parentB) override;
  /// Randomize this runner (rewrite its chromosomes by random data).
  virtual void Randomize() override;

  /// Print this runner to stream.
  virtual bool Print(FILE* stream = stdout) override;
  /// Scan this runner from stream.
  virtual bool Scan(FILE* stream = stdin) override;
};

// ------
